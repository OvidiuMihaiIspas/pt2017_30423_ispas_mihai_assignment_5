import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalField;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.lang.Object;
import java.text.DateFormat;

public class MonitoredData {

	String activityLabel,startTime,endTime;
	
	public MonitoredData(String activityLabel,String startTime,String endTime){
		this.activityLabel=activityLabel;
		this.startTime=startTime;
		this.endTime=endTime;
	}
	
	public String getActivityLabel(){
		return activityLabel;
	}
	
	public void setActivityLabel(String activity){
		this.activityLabel=activity;
	}
	
	public String getStartTime(){
		return startTime;
	}
	
	public void setStartTime(String time){
		this.startTime=time;
	}
	
	public String getEndTime(){
		return endTime;
	}
	
	public void setEndTime(String time){
		this.endTime=time;
	}
	
	public String getDay(String time){
		String[] part=time.split("\\s+");
		String day=part[0].substring(8, 10);
		return day;
	}
	
	public void nrDays(int cnt){
		String newDay,day="";
			newDay=getDay(getEndTime());
			if(!(newDay.equals(day))){
				cnt++;
			}
			day=newDay;
	}
	
	@Override
    public boolean equals(Object other)
    {
        // null check
        if (other == null)
        {
            return false;
        }
        // type check and cast
        if (getClass() != other.getClass())
        {
            return false;
        }

        // convert
        MonitoredData otherElement = (MonitoredData) other;
        // field comparison
        return this.getStartTime()==otherElement.getStartTime() && this.getEndTime() == ((MonitoredData) other).getEndTime() && this.getActivityLabel() == ((MonitoredData) other).getActivityLabel();

    }
	
	public long getDiff(){
		DateFormat p=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//TemporalField t = null;
		Date dt1 = null;
		try {
			dt1 = p.parse(getStartTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date dt2 = null;
		try {
			dt2 = p.parse(getEndTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long a=dt2.getTime()-dt1.getTime();
		return a;
	}
}
