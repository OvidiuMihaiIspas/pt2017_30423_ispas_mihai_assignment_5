import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProcessData {

	public static ArrayList<MonitoredData> list=new ArrayList<MonitoredData>();
	public static ArrayList<String> list1=new ArrayList<String>();
	public static Map<String, Long> x=new HashMap<String,Long>();
	public static Map<String, Long> y=new HashMap<String,Long>();
	public static Map<String, Long> z=new HashMap<String,Long>();
	public static Map<Integer,Map<String,Long>> t=new HashMap<Integer,Map<String,Long>>();
	public static Map<String,Long> s=new HashMap<String,Long>();
	public static Map<String,Long> m=new HashMap<String,Long>();
	public static Map<String,Long> n=new HashMap<String,Long>();
	public static Map<String,Long> g=new HashMap<String,Long>();
	public static Map<String,Long> h=new HashMap<String,Long>();
	public static Map<String,Long> j=new HashMap<String,Long>();
	
	public static void read(){
		FileReader fr;
		BufferedReader br;
		try {
			fr=new FileReader("E:/AN II/SEM II/PT/HWs/SmartHouse/Activities.txt");
			br=new BufferedReader(fr);
			String line;
			String activity,startTime,endTime;
			MonitoredData p;
			while((line=br.readLine())!=null){
				String[] part=line.split("\\s+");
				startTime=part[0]+" "+part[1];
				endTime=part[2]+" "+part[3];
				activity=part[4];
				p=new MonitoredData(activity,startTime,endTime);
				list.add(p);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void activitiesPerDay() throws FileNotFoundException{
		PrintWriter writer=new PrintWriter("Activities per days.txt");
		t=list.stream().collect(Collectors.groupingBy(activity->{
			String date=activity.getStartTime().split(" ")[0];
			String day=date.split("-")[2];
			return Integer.parseInt(day);
		},Collectors.groupingBy(activity->activity.getActivityLabel(),Collectors.counting())));
		for(int d:t.keySet()){
			writer.println("Day "+d);
			Map<String,Long> act=t.get(d);
			for(String s:act.keySet()){
				writer.println("                "+s+" "+act.get(s)+"\n");
			}
		}
		writer.close();
	}
	
	public static void distinctDays(){
		int nr=(int) list.stream().map(p->p.getDay(p.getEndTime())).distinct().count();
		System.out.println(nr);
	}
	
	public static void lessFiveMinutes() throws FileNotFoundException{
		PrintWriter writer=new PrintWriter("Less than five minutes.txt");
		g=list.stream().filter(p->{return convSecToMin(p.getDiff())<5;}).collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
		for(String s:g.keySet()){
			if((g.get(s)/x.get(s))>(9/10)){
				list1.add(s);
			}
		}
		for(String s:list1){
			writer.println(s);
		}
		writer.close();
	}
	
	public static void nrOccurences() throws FileNotFoundException{
		PrintWriter writer=new PrintWriter("Number of activities occurrences.txt");
		x=list.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
		for(String s:x.keySet()){
			writer.println(s+" : "+x.get(s));
		}
		writer.close();
	}
	
	public static Long convSecToTime(long sec){
		Long hours=TimeUnit.MILLISECONDS.toHours(sec);
		return hours;
	}
	public static Long convSecToMin(long sec){
		Long mins=TimeUnit.MILLISECONDS.toMinutes(sec);
		return mins;
	}
	
	public static void largerTenHours() throws FileNotFoundException{
		PrintWriter writer=new PrintWriter("Larget than 10 hours.txt");
		y=list.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.summingLong(p -> p.getDiff())));
		y.entrySet().stream().forEach(e->z.put(e.getKey(), convSecToTime(e.getValue())));
		m=z.entrySet().stream().filter(map->map.getValue()>10).collect(Collectors.toMap(p->p.getKey(), p->p.getValue()));
		for(String s:m.keySet()){
			writer.println(s+ " "+m.get(s));
		}
		writer.close();
	}
	
	public static long diff(Date d1,Date d2){
		long diff=d2.getTime()-d1.getTime();
		long diff1=diff;
		return diff1;
	}
	
	public static String newDay,day="";
	public static int cnt=0;
	public static void main(String[] args) throws FileNotFoundException, ParseException{
		read();
		
		distinctDays();
		
		nrOccurences();
		
		largerTenHours();
		
		activitiesPerDay();
		
		lessFiveMinutes();
		
	}
}
